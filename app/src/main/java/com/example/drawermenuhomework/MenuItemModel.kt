package com.example.drawermenuhomework

class MenuItemModel(
    val icon: Int,
    val title: String
)