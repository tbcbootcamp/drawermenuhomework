package com.example.drawermenuhomework

interface ItemOnClick {
    fun onClick(position: Int)
}