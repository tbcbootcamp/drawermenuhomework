package com.example.drawermenuhomework

import android.os.Bundle
import android.view.Menu
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.toolbar_layout.*

class MainActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var menuRecyclerViewAdapter: MenuRecyclerViewAdapter
    private val items = mutableListOf<MenuItemModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initMenuItems()
        init()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    private fun init() {
        setSupportActionBar(toolbar)
        val navController = findNavController(R.id.nav_host_fragment)
        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.nav_home, R.id.nav_gallery, R.id.nav_slideshow
            ), drawer_layout
        )
        appBarConfiguration = AppBarConfiguration(navController.graph, drawer_layout)

        setupActionBarWithNavController(navController, appBarConfiguration)
        nav_view.setupWithNavController(navController)

    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(drawer_layout) || super.onSupportNavigateUp()
    }

    private fun initMenuItems() {

        menuRecyclerViewAdapter = MenuRecyclerViewAdapter(items, this)
//        menuRecyclerViewAdapter = MenuRecyclerViewAdapter(items, object : ItemOnClick){
//            override fun onClick(position: Int) {}
//        }
        menuRecyclerView.layoutManager = LinearLayoutManager(this)
        menuRecyclerView.adapter = menuRecyclerViewAdapter
        setMenuItems()
    }

    private fun setMenuItems(){
        items.add(MenuItemModel(R.drawable.ic_menu_camera, "camera"))
        items.add(MenuItemModel(R.drawable.ic_menu_gallery, "gallery"))
        items.add(MenuItemModel(R.drawable.ic_menu_slideshow, "slideshow"))
    }
}